#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(future_join)]
#![feature(exclusive_range_pattern)]

extern crate alloc;

use display_interface_spi::SPIInterface;
use alloc::vec;
use alloc::vec::Vec;
use alloc::string::ToString;
use core::alloc::GlobalAlloc;
use core::cell::RefCell;
use core::fmt::Write;
use core::str::FromStr;

use byteorder::ByteOrder;
use chrono::{Datelike, Timelike};
use embassy_embedded_hal::SetConfig;
use embassy_embedded_hal::shared_bus::{asynch, blocking};
use embassy_executor::Spawner;
use embassy_net::{Config, Stack, StackResources};
use embassy_sync::{blocking_mutex, mutex};
use embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;
use embassy_sync::signal::Signal;
use embassy_time::{Delay, Duration, Instant, Ticker, Timer};
use embedded_graphics::draw_target::DrawTarget;
use embedded_graphics::Drawable;
use embedded_graphics::geometry::{OriginDimensions, Point, Size};
use embedded_graphics::image::Image;
use embedded_graphics::pixelcolor::RgbColor;
use embedded_graphics::prelude::{RawData, Transform};
use embedded_graphics::primitives::Primitive;
use embedded_graphics::text::renderer::TextRenderer;
use embedded_hal::digital::OutputPin;
use embedded_hal_async::digital::Wait;
use embedded_hal_async::i2c::I2c;
use embedded_iconoir::icons;
use embedded_iconoir::prelude::IconoirNewIcon;
use embedded_sdmmc::{DirEntry, SdCard};
use embedded_svc::wifi::Wifi;
use esp32_utils_crate::dummy_pin::DummyPin;
use esp32_utils_crate::fonts::CharacterStyles;
use esp32_utils_crate::ft6236_asynch::{FT6236, FT6236_DEFAULT_ADDR};
use esp32_utils_crate::ft6236_asynch::EventType::PressDown;
use esp32_utils_crate::graphics::{GraphicUtils, Progress, Theme};
use esp32_utils_crate::png::Png;
use esp32_utils_crate::sdcard::SdcardManager;
use esp32_utils_crate::tsc2007::TSC2007_ADDR;
use esp_backtrace;
use esp_hal::{clock::ClockControl, embassy, IO, peripherals::Peripherals, prelude::*, psram};
use esp_hal::{Rng, timer::TimerGroup};
use esp_hal::clock::Clocks;
use esp_hal::gpio::{GpioPin, NO_PIN, Unknown};
use esp_hal::i2c::I2C;
use esp_hal::ledc::{channel, LEDC, LowSpeed, LSGlobalClkSource, timer};
use esp_hal::peripheral::Peripheral;
use esp_hal::peripherals::I2C0;
use esp_hal::rmt::TxChannelCreator;
use esp_hal::spi::master::{Instance, Spi};
use esp_hal::spi::SpiMode;
use esp_println::println;
use esp_wifi::{EspWifiInitFor, initialize};
use esp_wifi::wifi::{ClientConfiguration, Configuration, WifiController, WifiDevice, WifiEvent, WifiStaDevice, WifiState};
use heapless::String;
use micromath::F32Ext;
use mipidsi::Builder;
use mipidsi::models::ILI9341Rgb565;
use mipidsi::options::{ColorOrder, Orientation, Rotation};
use serde::{Deserialize, Serialize};
use static_cell::{make_static, StaticCell};
use zune_png::PngDecoder;
use zune_png::zune_core::options::DecoderOptions;

use crate::weather::{WeatherAll, WeatherIcons, WeatherProvider};

mod rolling_box;
mod bouncing_box;
mod weather;
#[global_allocator]
static ALLOCATOR: esp_alloc::EspHeap = esp_alloc::EspHeap::empty();

fn init_psram_heap() {
    unsafe {
        ALLOCATOR.init(psram::psram_vaddr_start() as *mut u8, psram::PSRAM_BYTES);
    }
}

// #[global_allocator]
// static ALLOCATOR: esp_alloc::EspHeap = esp_alloc::EspHeap::empty();
//
// fn init_heap() {
//     const HEAP_SIZE: usize = 32 * 1024;
//     static mut HEAP: MaybeUninit<[u8; HEAP_SIZE]> = MaybeUninit::uninit();
//
//     unsafe {
//         ALLOCATOR.init(HEAP.as_mut_ptr() as *mut u8, HEAP_SIZE);
//     }
// }

static mut APP_CORE_STACK: esp_hal::cpu_control::Stack<8192> = esp_hal::cpu_control::Stack::new();

const SSID: &str = env!("SSID");
const PASSWORD: &str = env!("PASSWORD");

const GFORCE: f32 = 9.81;

const SCREEN_TIMEOUT_SECS: u64 = 30;
const SCREEN_BRIGHTNESS_PERCENT: u8 = 30;

macro_rules! singleton {
    ($val:expr, $typ:ty) => {{
        static STATIC_CELL: StaticCell<$typ> = StaticCell::new();
        STATIC_CELL.init($val)
    }};
}

#[derive(Clone, Debug)]
struct TouchData {
    x: u16,
    y: u16,
    z: u16,
}

impl TouchData {
    fn new(x: u16, y: u16, z: u16) -> Self {
        TouchData {
            x,
            y,
            z,
        }
    }
}

static TOUCH_DATA_SIGNAL: Signal<CriticalSectionRawMutex, TouchData> = Signal::new();

#[embassy_executor::task]
async fn handle_tp_touch_ft6206(i2c: asynch::i2c::I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0>>, tp_irq_pin: GpioPin<Unknown, 38>,
                                orientation: Rotation, width: u16, height: u16) {
    let mut ft6206 = FT6236::new(i2c);
    let mut tp_irq_input = tp_irq_pin.into_pull_down_input();

    loop {
        tp_irq_input.wait_for_low().await.unwrap();
        if let Ok(point) = ft6206.get_point0().await {
            if point.is_some() {
                let point_event = point.unwrap();
                let x = point_event.x;
                let y = point_event.y;
                let z = point_event.weight;
                if point_event.event == PressDown {
                    let (x, y) = match orientation {
                        Rotation::Deg0 => (width - x, height - y),
                        Rotation::Deg90 => (width - y, x),
                        Rotation::Deg180 => (x, y),
                        Rotation::Deg270 => (y, height - x)
                    };
                    println!("{}x{}", x, y);

                    TOUCH_DATA_SIGNAL.signal(TouchData::new(x, y, z.into()));
                    // debounce
                    Timer::after(Duration::from_millis(100)).await
                }
            }
        }
    }
}

#[embassy_executor::task]
async fn net_task(stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>) {
    stack.run().await
}

#[embassy_executor::task]
async fn connection(mut controller: WifiController<'static>) {
    loop {
        match esp_wifi::wifi::get_wifi_state() {
            WifiState::StaConnected => {
                // wait until we're no longer connected
                controller.wait_for_event(WifiEvent::StaDisconnected).await;
                Timer::after(Duration::from_millis(5000)).await
            }
            _ => {}
        }
        if !matches!(controller.is_started(), Ok(true)) {
            let client_config = Configuration::Client(ClientConfiguration {
                ssid: SSID.try_into().unwrap(),
                password: PASSWORD.try_into().unwrap(),
                ..Default::default()
            });
            controller.set_configuration(&client_config).unwrap();
            println!("Starting wifi");
            controller.start().await.unwrap();
            println!("Wifi started!");
        }

        match controller.connect().await {
            Ok(_) => println!("Wifi connected!"),
            Err(e) => {
                println!("Failed to connect to wifi: {e:?}");
                Timer::after(Duration::from_millis(5000)).await
            }
        }
    }
}

static WEATHER_DATA_SIGNAL: Signal<CriticalSectionRawMutex, WeatherAll> = Signal::new();

#[embassy_executor::task]
async fn handle_weather_task(stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>) {
    let mut ticker = Ticker::every(Duration::from_secs(60 * 60));
    let weather_provider = WeatherProvider::new();
    loop {
        if let Ok(weather) = weather_provider.fetch_weather(stack).await {
            WEATHER_DATA_SIGNAL.signal(weather);
        }

        ticker.next().await;
    }
}

#[derive(Clone, Debug)]
/// This is a simple moving average filter.
pub struct Smooth {
    n: usize,
    readings: Vec<i32>,
    read_index: usize,
    average: i32,
}

impl Smooth {
    /// Create a new Smooth filter with the given number of readings.
    pub fn new(num_readings: usize) -> Self {
        Smooth {
            n: num_readings,
            readings: vec![0; num_readings],
            read_index: 0,
            average: 0,
        }
    }
    /// Add a new reading to the filter.
    pub fn add_reading(&mut self, reading: i32) {
        self.readings[self.read_index] = reading;

        self.read_index += 1;
        self.read_index %= self.n;

        self.average = self.readings.iter().fold(0, |s, &x| s + x) / self.n as i32;
    }

    /// Get the current average reading.
    pub fn get_average(&self) -> i32 {
        self.average
    }
}

#[derive(Clone, Debug)]
/// This is a simple moving average filter.
pub struct LowPass {
    last_reading: i32,
}

impl LowPass {
    pub fn new() -> Self {
        LowPass {
            last_reading: 0,
        }
    }
    /// Add a new reading to the filter.
    pub fn add_reading(&mut self, reading: i32) -> i32 {
        let last_reading = self.last_reading;
        self.last_reading = reading;
        if last_reading == 0 {
            return reading;
        }
        (last_reading as f32 * 0.9 + reading as f32 * 0.1) as i32
    }
}

pub fn compute_roll(acc_y: f32, acc_z: f32) -> f32 {
    acc_y.atan2(acc_z).to_degrees()
}

pub fn compute_pitch(acc_x: f32, acc_y: f32, acc_z: f32) -> f32 {
    (-acc_x / (acc_y * acc_y + acc_z * acc_z).sqrt()).atan().to_degrees()
}

pub fn compute_ball_accel(angle: i32) -> f32 {
    GFORCE * ((angle as f32).to_radians().sin())
}

fn round_temp_display(temp: f32) -> f32 {
    (((temp * 10.0) as u16) as f32) / 10.0
}

fn round_temp_display_coarse(temp: f32) -> u16 {
    temp.round() as u16
}


fn center_height_relative(height1: u32, height2: u32) -> i32 {
    (height1 / 2 - height2 / 2) as i32
}

fn center_width_relative(width1: u32, width2: u32) -> i32 {
    (width1 / 2 - width2 / 2) as i32
}

#[main]
async fn main(spawner: Spawner) -> ! {
    // init_heap();
    let peripherals = Peripherals::take();

    psram::init_psram(peripherals.PSRAM);
    init_psram_heap();

    let system = peripherals.SYSTEM.split();
    let clocks = singleton!(
        ClockControl::max(system.clock_control).freeze(),
        Clocks
    );
    let mut buf: String<1024> = String::new();

    // [env]
    // ESP_LOGLEVEL="INFO"
    esp_println::logger::init_logger_from_env();

    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    embassy::init(&clocks, timer_group0);

    let timer_group1 = TimerGroup::new(peripherals.TIMG1, &clocks);
    let mut rng = Rng::new(peripherals.RNG);
    let _init = initialize(
        EspWifiInitFor::Wifi,
        timer_group1.timer0,
        rng,
        system.radio_clock_control,
        &clocks,
    ).unwrap();

    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    // backlight
    let bl_pin = io.pins.gpio5.into_push_pull_output();
    let mut ledc = LEDC::new(peripherals.LEDC, &clocks);

    ledc.set_global_slow_clock(LSGlobalClkSource::APBClk);

    let mut lstimer0 = ledc.get_timer::<LowSpeed>(timer::Number::Timer0);

    lstimer0
        .configure(timer::config::Config {
            duty: timer::config::Duty::Duty5Bit,
            clock_source: timer::LSClockSource::APBClk,
            frequency: 24u32.kHz(),
        })
        .unwrap();

    let mut channel0 = ledc.get_channel(channel::Number::Channel0, bl_pin);
    channel0
        .configure(channel::config::Config {
            timer: &lstimer0,
            duty_pct: SCREEN_BRIGHTNESS_PERCENT,
            pin_config: channel::config::PinConfig::PushPull,
        })
        .unwrap();

    let mut i2c0 = I2C::new(
        peripherals.I2C0,
        io.pins.gpio8,
        io.pins.gpio9,
        400u32.kHz(),
        clocks,
    );

    // let mut i2c1 = I2C::new(
    //     peripherals.I2C1,
    //     io.pins.gpio15,
    //     io.pins.gpio16,
    //     400u32.kHz(),
    //     clocks,
    // );

    let i2c0_bus = mutex::Mutex::<blocking_mutex::raw::CriticalSectionRawMutex, _>::new(i2c0);
    let i2c0_bus_static = make_static!(i2c0_bus);
    //
    // let i2c1_bus = mutex::Mutex::<blocking_mutex::raw::CriticalSectionRawMutex, _>::new(i2c1);
    // let i2c1_bus_static = make_static!(i2c1_bus);
    //
    let mut i2c0_dev0 = asynch::i2c::I2cDevice::new(i2c0_bus_static);
    let mut i2c0_dev1 = asynch::i2c::I2cDevice::new(i2c0_bus_static);

    let mut dummy = [0u8];

    let has_tsc2007 = embedded_hal_async::i2c::I2c::read(&mut i2c0_dev0, TSC2007_ADDR, &mut dummy).await.is_ok();
    println!("has_tsc2007 = {}", has_tsc2007);

    let has_ft6206 = embedded_hal_async::i2c::I2c::read(&mut i2c0_dev0, FT6236_DEFAULT_ADDR, &mut dummy).await.is_ok();
    println!("has_ft6206 = {}", has_ft6206);

    let has_imu = embedded_hal_async::i2c::I2c::read(&mut i2c0_dev0, 0x69, &mut dummy).await.is_ok();
    println!("has_imu = {}", has_imu);

    // for addr in 1..=127 {
    //     // println!("Scanning Address i2c0 {}", addr as u8);
    //
    //     // Scan Address
    //     let res = embedded_hal_async::i2c::I2c::read(&mut i2c0_dev0, addr as u8, &mut dummy);
    //
    //     // Check and Print Result
    //     match res.await {
    //         Ok(_) => println!("Device Found at Address {}", addr as u8),
    //         Err(_) => { },
    //     }
    // }
    // for addr in 1..=127 {
    //     println!("Scanning Address i2c1 {}", addr as u8);
    //
    //     // Scan Address
    //     let res = embedded_hal_async::i2c::I2c::read(&mut i2c1, addr as u8, &mut dummy);
    //
    //     // Check and Print Result
    //     match res.await {
    //         Ok(_) => println!("Device Found at Address {}", addr as u8),
    //         Err(_) => println!("No Device Found"),
    //     }
    // }


    let sclk = io.pins.gpio36;
    let mosi = io.pins.gpio35;
    let miso = io.pins.gpio37;
    let dc = io.pins.gpio12.into_push_pull_output();

    let display_cs = io.pins.gpio6.into_push_pull_output();
    let sd_cs = io.pins.gpio1.into_push_pull_output();

    let spi2 = Spi::new(peripherals.SPI2, 20u32.MHz(), SpiMode::Mode0, clocks)
        .with_pins(Some(sclk), Some(mosi), Some(miso), NO_PIN);

    let spi2_bus = blocking_mutex::Mutex::<blocking_mutex::raw::CriticalSectionRawMutex, _>::new(RefCell::new(spi2));
    let spi2_bus_static = make_static!(spi2_bus);

    let display_spi = blocking::spi::SpiDevice::new(
        spi2_bus_static,
        display_cs);
    let sd_spi = blocking::spi::SpiDevice::new(
        spi2_bus_static,
        DummyPin);

    let spi_iface = SPIInterface::new(display_spi, dc);

    // Define the reset and write enable pins as digital outputs and make them high
    // let mut rst = io.pins
    //     .gpio11
    //     .into_push_pull_output();
    // rst.set_output_high(true);

    // let mut wr = io.pins
    //     .gpio10
    //     .into_push_pull_output();
    // wr.set_output_high(true);
    //
    // // Define the Data/Command select pin as a digital output
    // let mut dc = io.pins.gpio7.into_push_pull_output();
    // dc.set_output_high(true);
    //
    // // Define the pins used for the parallel interface as digital outputs
    // let lcd_d0 = io.pins.gpio17.into_push_pull_output();
    // let lcd_d1 = io.pins.gpio18.into_push_pull_output();
    // let lcd_d2 = io.pins.gpio14.into_push_pull_output();
    // let lcd_d3 = io.pins.gpio12.into_push_pull_output();
    // let lcd_d4 = io.pins.gpio6.into_push_pull_output();
    // let lcd_d5 = io.pins.gpio5.into_push_pull_output();
    // let lcd_d6 = io.pins.gpio36.into_push_pull_output();
    // let lcd_d7 = io.pins.gpio35.into_push_pull_output();
    //
    // // Define the parallel bus with the previously defined parallel port pins
    // let bus = Generic8BitBus::new((
    //     lcd_d0, lcd_d1, lcd_d2, lcd_d3, lcd_d4, lcd_d5, lcd_d6, lcd_d7,
    // ));

    // Define the display interface from a generic 8 bit bus, a Data/Command select pin and a write enable pin
    // let di_iface = PGPIO8BitInterface::new(bus, dc, wr);

    let mut delay = Delay;

    let mut display = Builder::new(ILI9341Rgb565, spi_iface)
        .orientation(Orientation::new().rotate(Rotation::Deg270).flip_horizontal())
        .color_order(ColorOrder::Bgr)
        .display_size(240, 320)
        .init(&mut delay).unwrap();

    let display_width = display.size().width;
    let display_height = display.size().height;
    let theme = Theme::new_dark_theme();
    let mut character_styles = CharacterStyles::new_with_color(theme.text_color_primary);
    character_styles.set_background_color(theme.screen_background_color);

    let wifi_icon = icons::size96px::connectivity::Wifi::new(theme.text_color_primary);
    let mut progress = Progress::new(&wifi_icon, "Connecting to Wifi...",
                                     Point::new(0, 0), Size::new(display_width, display_height),
                                     theme.screen_background_color, character_styles.default_character_style(),
                                     &theme);

    let sdcard = SdCard::new(sd_spi, sd_cs, Delay);
    let mut sdcard_manager = make_static!(SdcardManager::new(sdcard));

    let mut root_file_list: Vec<DirEntry> = Vec::new();
    if let Ok(()) = sdcard_manager.open_root_dir() {
        if sdcard_manager.get_root_dir_entries(&mut root_file_list).is_err() {
            root_file_list.clear();
        }
    }
    println!("root dir files size = {}", root_file_list.len());

    let mut weather_icons_file_list: Vec<DirEntry> = Vec::new();
    if sdcard_manager.get_subdir_entries("large", &mut weather_icons_file_list).is_err() {
        weather_icons_file_list.clear();
    }

    println!("weather icon files size = {}", weather_icons_file_list.len());
    let weather_icons = WeatherIcons::new();

    progress.update_text(&mut display, "Connecting to Wifi...").unwrap();

    let wifi = peripherals.WIFI;
    let (wifi_interface, controller) =
        esp_wifi::wifi::new_with_mode(&_init, wifi, WifiStaDevice).unwrap();

    let config = Config::dhcpv4(Default::default());

    // Init network stack
    let net_stack = Stack::new(
        wifi_interface,
        config,
        make_static!(StackResources::<3>::new()),
        rng.random() as u64,
    );
    let stack = make_static!(net_stack);

    spawner.must_spawn(connection(controller));
    spawner.must_spawn(net_task(stack));

    loop {
        if stack.is_link_up() {
            break;
        }
        Timer::after(Duration::from_millis(500)).await;
    }

    progress.update_text(&mut display, "Waiting for IP address...").unwrap();

    loop {
        if let Some(config) = stack.config_v4() {
            println!("Got IP: {}", config.address);
            break;
        }
        Timer::after(Duration::from_millis(500)).await;
    }

    // if has_tsc2007 {
    //     spawner.must_spawn(handle_tp_touch_tsc2007(i2c0_dev0, io.pins.gpio38, display_orientation, display_width as u16, display_height as u16));
    if has_ft6206 {
        spawner.must_spawn(handle_tp_touch_ft6206(i2c0_dev0, io.pins.gpio38, display.orientation().rotation, display_width as u16, display_height as u16));
    }

    spawner.must_spawn(handle_weather_task(stack));

    let mut current_screen = 0;

    display.clear(theme.screen_background_color).unwrap();

    // let mut cpu_control = CpuControl::new(system.cpu_control);
    // let _guard = cpu_control
    //     .start_app_core(unsafe { &mut APP_CORE_STACK }, move || {
    //         let executor = make_static!(Executor::new());
    //         executor.run(|spawner| {
    //             spawner.spawn(handle_tp_touch_ft6206(i2c0_dev0, io.pins.gpio38, display_orientation, display_width as u16, display_height as u16)).ok();
    //         })
    //     })
    //     .unwrap();


    // if has_imu {
    //     let mut imu = Icm20948::new_i2c(i2c0_dev1, delay)
    //         .gyr_unit(GyrUnit::Rps)   // Set gyroscope to output rad/s
    //         .gyr_dlp(GyrDlp::Hz196)   // Set gyroscope low-pass filter
    //         .acc_range(AccRange::Gs8) // Set accelerometer measurement range
    //         .set_address(0x69)        // Set address (0x68 or 0x69)
    //         .initialize_9dof().await.unwrap(); // Initialize with magnetometer
    //
    //     let display_width: i32 = display.width() as i32;
    //     let display_height: i32 = display.height() as i32;
    //
    //     let clear_style = PrimitiveStyleBuilder::new()
    //         .fill_color(Rgb565::BLACK)
    //         .build();
    //
    //     let color_style = PrimitiveStyleBuilder::new()
    //         .fill_color(Rgb565::RED)
    //         .build();
    //
    //     let colors: [Rgb565; 6] = [Rgb565::RED,
    //         Rgb565::GREEN,
    //         Rgb565::BLUE,
    //         Rgb565::CYAN,
    //         Rgb565::MAGENTA,
    //         Rgb565::YELLOW];
    //
    //     let size: i32 = 40;
    //     let sizes: [i32; 4] = [size - 10, size - 8, size - 5, size];
    //     let weights: [f32; 4] = [2., 2.5, 3.0, 3.5];
    //     let start_pos: [Point; 4] = [Point::new(0, 0),
    //         Point::new(display_width - size, 0),
    //         Point::new(0, display_height - size),
    //         Point::new(display_width - size, display_height - size)];
    //     let mut arena = vec![vec![false; display_height as usize]; display_width as usize];
    //
    //     let mut boxes = Vec::new();
    //     let mut colors_styles = Vec::new();
    //
    //     for i in 0..1 {
    //         boxes.push(rolling_box::Box::new(display_width, display_height, sizes[i], colors[i], weights[i], start_pos[i], &mut arena));
    //         colors_styles.push(PrimitiveStyleBuilder::new()
    //             .fill_color(colors[i])
    //             .build());
    //     }
    //
    //     let mut angle_x_filter = LowPass::new();
    //     let mut angle_y_filter = LowPass::new();
    //
    //     let mut time = Instant::now();
    //
    //     // let mut fusion = Fusion::new(0.05, 20., 50);
    //     // fusion.set_mode(Mode::Dof6);
    //
    //     loop {
    //         let Ok(measurement) = imu.read_9dof().await else { continue; };
    //
    //         let acc_x = measurement.acc.x;
    //         let acc_y = measurement.acc.y;
    //         let acc_z = measurement.acc.z;
    //
    //         // let gyro_x = measurement.gyr.x;
    //         // let gyro_y = measurement.gyr.y;
    //         // let gyro_z = measurement.gyr.z;
    //
    //         // Collect outputs
    //         let roll_acc = compute_roll(acc_y, acc_z);
    //         let pitch_acc = compute_pitch(acc_x, acc_y, acc_z);
    //
    //         let delta_time_secs = time.elapsed().as_millis() as f32 / 1000.0;
    //         time = Instant::now();
    //
    //         // fusion.set_data_dof6(acc_x, acc_y, acc_z, gyro_x, gyro_y, gyro_z);
    //
    //         // Perform a step of the algorithm
    //         // fusion.step(delta_time_secs);
    //         // // println!("{} {} {}, {}", roll_acc, pitch_acc, fusion.kalman_x_angle, fusion.kalman_y_angle);
    //         // let roll_acc = fusion.kalman_x_angle;
    //         // let pitch_acc = fusion.kalman_y_angle;
    //
    //         // 90 -> + 180 / -180 -> -90
    //         // change to 90 - 0 - 90
    //         let angle_x_acc = (roll_acc.abs() - 180.0).abs();
    //         let angle_y_acc = pitch_acc.abs();
    //
    //         let angle_x = angle_x_filter.add_reading(angle_x_acc as i32);
    //         let angle_y = angle_y_filter.add_reading(angle_y_acc as i32);
    //
    //         println!("{} - {}", angle_x, angle_y);
    //
    //         let accel_x = compute_ball_accel(angle_x);
    //         let accel_y = compute_ball_accel(angle_y);
    //
    //         let mut i = 0;
    //         for b in boxes.iter_mut() {
    //             b.update_pos(pitch_acc as i32, roll_acc as i32, accel_x, accel_y, &mut arena, time.elapsed().as_millis());
    //
    //             Circle::new(Point::new(b.prev_x, b.prev_y), b.size as u32)
    //                 .into_styled(clear_style)
    //                 .draw(&mut display).unwrap();
    //
    //             Circle::new(Point::new(b.x, b.y), b.size as u32)
    //                 .into_styled(colors_styles[i])
    //                 .draw(&mut display).unwrap();
    //             i = i + 1;
    //         }
    //         Timer::after(Duration::from_millis(50)).await
    //     }
    // } else {
    // let colors: [Rgb565; 8] = [Rgb565::RED,
    //     Rgb565::GREEN,
    //     Rgb565::BLUE,
    //     Rgb565::CYAN,
    //     Rgb565::MAGENTA,
    //     Rgb565::YELLOW,
    //     Rgb565::WHITE,
    //     Rgb565::new(0xff, 0x4f, 0)];
    //
    // let sizes: [i16; 8] = [14, 13, 12, 11, 10, 9, 8, 7];
    // let mut boxes = Vec::new();
    // let mut colors_styles = Vec::new();
    //
    // for i in 0..8 {
    //     let r = rng.random() as i16 % 10;
    //     let x_speed = if r < 5 { 5 - r } else { r - 10 };
    //     let r = rng.random() as i16 % 10;
    //     let y_speed = if r < 5 { 5 - r } else { r - 10 };
    //     boxes.push(bouncing_box::Box::new(display_width as i16, display_height as i16, sizes[i], colors[i], x_speed, y_speed));
    //     colors_styles.push(PrimitiveStyleBuilder::new()
    //         .fill_color(colors[i])
    //         .build());
    // }
    //
    // let clear_style = PrimitiveStyleBuilder::new()
    //     .fill_color(theme.screen_background_color)
    //     .build();
    //
    // loop {
    //     let mut i = 0;
    //     for b in boxes.iter_mut() {
    //         b.update_pos();
    //
    //         Rectangle::new(Point::new(b.prev_x as i32, b.prev_y as i32), Size::new(b.size as u32, b.size as u32))
    //             .into_styled(clear_style)
    //             .draw(&mut display).unwrap();
    //
    //         Rectangle::new(Point::new(b.x as i32, b.y as i32), Size::new(b.size as u32, b.size as u32))
    //             .into_styled(colors_styles[i])
    //             .draw(&mut display).unwrap();
    //         i = i + 1;
    //     }
    //     Timer::after(Duration::from_millis(50)).await;
    //
    //     if TOUCH_DATA_SIGNAL.signaled() {
    //         let touch_data = TOUCH_DATA_SIGNAL.wait().await;
    //         println!("{:?}", touch_data)
    //     }
    // }
    // }

    let mut display_off = false;
    let mut time = Instant::now();

    loop {
        if TOUCH_DATA_SIGNAL.signaled() {
            time = Instant::now();

            let touch_data = TOUCH_DATA_SIGNAL.wait().await;
            let touch_point = Point::new(touch_data.x as i32, touch_data.y as i32);

            if display_off {
                if let Ok(()) = channel0.set_duty(SCREEN_BRIGHTNESS_PERCENT) {
                    display_off = false;
                    continue;
                }
            }
        }

        if WEATHER_DATA_SIGNAL.signaled() {
            let json = WEATHER_DATA_SIGNAL.wait().await;

            let mut current_icon = json.current.weather[0].icon.parse().unwrap();
            let mut current_id = json.current.weather[0].id;
            let mut current_icon_name = weather_icons.get_condition_icon_file(current_icon, current_id);
            let small_weather_icon_size: u32 = 64;
            let large_weather_icon_size: u32 = 90;
            let large_weather_icon_padding: u32 = 10;

            buf.clear();
            write!(&mut buf, "  {} C  ", round_temp_display_coarse(json.current.temp)).unwrap();
            GraphicUtils::display_text(&mut display, Point::new((large_weather_icon_size + large_weather_icon_padding) as i32,
                                                                center_height_relative(large_weather_icon_size,
                                                                                       character_styles.medium_character_style().font.character_size.height)),
                                       character_styles.medium_character_style(), character_styles.default_text_style(), buf.as_str()).unwrap();

            for dir_entry in weather_icons_file_list.iter() {
                if dir_entry.name.to_string() == current_icon_name.as_str() {
                    let mut image_buffer_vec = vec![0; dir_entry.size as usize];
                    let load_result = sdcard_manager.load_subdir_file_into_buffer("large", current_icon_name.as_str(), &mut image_buffer_vec);
                    if load_result.is_ok() {
                        let mut options = DecoderOptions::default();
                        options = options.png_set_strip_to_8bit(true);

                        let mut decoder = PngDecoder::new_with_options(image_buffer_vec, options);
                        let pixels_result = decoder.decode_raw();
                        if pixels_result.is_ok() {
                            let (width, height) = decoder.get_dimensions().unwrap();
                            let pixels = pixels_result.unwrap();
                            let png = Png::new(&pixels, Size::new(width as u32, height as u32), decoder.get_colorspace().unwrap(), theme.screen_background_color);
                            let image = Image::new(&png, Point::new(0, 0));
                            image.draw(&mut display).unwrap();
                        } else {
                            println!("{:?}", pixels_result.err());
                        }
                    } else {
                        println!("{:?}", load_result.err());
                    }
                }
            }

            let forecast_days = 4;
            let small_weather_icon_start_height: u32 = 110;
            let small_weather_icon_height_padding: u32 = 5;
            let small_weather_icon_width = display_width / forecast_days;
            let small_weather_icon_start_padding = (small_weather_icon_width - small_weather_icon_size) / 2;
            let small_weather_icon_height = small_weather_icon_size + 2 * small_weather_icon_height_padding;


            for i in 0..forecast_days {
                let day_json = &json.days[i as usize];
                current_icon = day_json.weather[0].icon.parse().unwrap();
                current_id = day_json.weather[0].id;
                current_icon_name = weather_icons.get_condition_icon_file(current_icon, current_id);
                let temp_min = round_temp_display_coarse(day_json.temp.min);
                let temp_max = round_temp_display_coarse(day_json.temp.max);

                let small_weather_icon_box_start = i as u32 * small_weather_icon_width;
                let small_weather_text_center = small_weather_icon_box_start + small_weather_icon_width / 2;

                buf.clear();
                write!(&mut buf, " {}/{} C ", temp_min, temp_max).unwrap();
                GraphicUtils::display_text(&mut display, Point::new(small_weather_text_center as i32, (small_weather_icon_start_height + small_weather_icon_height) as i32), character_styles.small_character_style(), character_styles.center_aligned_text_style(), buf.as_str()).unwrap();

                for dir_entry in weather_icons_file_list.iter() {
                    if dir_entry.name.to_string() == current_icon_name.as_str() {
                        let mut image_buffer_vec = vec![0; dir_entry.size as usize];
                        let load_result = sdcard_manager.load_subdir_file_into_buffer("small", current_icon_name.as_str(), &mut image_buffer_vec);
                        if load_result.is_ok() {
                            let mut options = DecoderOptions::default();
                            options = options.png_set_strip_to_8bit(true);

                            let mut decoder = PngDecoder::new_with_options(image_buffer_vec, options);
                            let pixels_result = decoder.decode_raw();
                            if pixels_result.is_ok() {
                                let (width, height) = decoder.get_dimensions().unwrap();
                                let pixels = pixels_result.unwrap();
                                let png = Png::new(&pixels, Size::new(width as u32, height as u32), decoder.get_colorspace().unwrap(), theme.screen_background_color);
                                let image = Image::new(&png, Point::new((small_weather_icon_box_start + small_weather_icon_start_padding) as i32, small_weather_icon_start_height as i32));
                                image.draw(&mut display).unwrap();
                            } else {
                                println!("{:?}", pixels_result.err());
                            }
                        } else {
                            println!("{:?}", load_result.err());
                        }
                    }
                }
            }
        }
        if !display_off {
            if time.elapsed().as_secs() >= SCREEN_TIMEOUT_SECS {
                if let Ok(()) = channel0.set_duty(0) {
                    display_off = true;
                }
            }
        }
        Timer::after(Duration::from_millis(100)).await
    }
}
