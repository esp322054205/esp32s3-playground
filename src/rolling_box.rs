use alloc::vec::Vec;

use embedded_graphics::geometry::Point;
use embedded_graphics::pixelcolor::Rgb565;
use embedded_graphics::primitives::{Circle, ContainsPoint};
use esp_println::println;
use micromath::F32Ext;

pub struct Box {
    pub size: i32,
    w: i32,
    h: i32,
    pub color: Rgb565,
    pub weight: f32,
    pub x: i32,
    pub y: i32,
    pub prev_x: i32,
    pub prev_y: i32,
    prev_time_millis: u64,
    prev_accel_x: f32,
    prev_accel_y: f32,
    prev_pitch: i32,
    prev_roll: i32
}

impl Box {
    pub fn new(width: i32, height: i32, size: i32, color: Rgb565, weight: f32, start_pos: Point, arena: &mut Vec<Vec<bool>>) -> Self {
        let mut b = Box {
            size,
            w: width,
            h: height,
            color,
            weight,
            x: 0,
            y: 0,
            prev_x: 0,
            prev_y: 0,
            prev_time_millis: 0,
            prev_accel_x: 0.0,
            prev_accel_y: 0.0,
            prev_pitch: 0,
            prev_roll: 0,
        };

        b.x = start_pos.x;
        b.y = start_pos.y;
        b.prev_x = b.x;
        b.prev_y = b.y;

        b.set_arena_status(true, arena);
        b
    }
    fn set_arena_status(&mut self, value: bool, arena: &mut Vec<Vec<bool>>) {
        let c = Circle::new(Point::new(self.x, self.y), self.size as u32);
        for i in self.x..self.x + self.size {
            for j in self.y..self.y + self.size {
                if c.contains(Point::new(i, j)) {
                    arena[i as usize][j as usize] = value;
                }
            }
        }
    }

    fn get_free_move_x_plus(&mut self, arena: &mut Vec<Vec<bool>>) -> i32 {
        if self.x == self.w - self.size { return 0; }

        let mut max_x = 0;
        for i in self.x + self.size..self.w {
            for j in self.y..self.y + self.size {
                if arena[i as usize][j as usize] { return max_x; }
            }
            max_x = max_x + 1;
        }
        max_x
    }

    fn get_free_move_x_minus(&mut self, arena: &mut Vec<Vec<bool>>) -> i32 {
        if self.x == 0 { return 0; }

        let mut max_x = 0;
        for i in (0..self.x).rev() {
            for j in self.y..self.y + self.size {
                if arena[i as usize][j as usize] { return max_x; }
            }
            max_x = max_x + 1;
        }
        max_x
    }

    fn get_free_move_y_plus(&mut self, arena: &mut Vec<Vec<bool>>) -> i32 {
        if self.y == self.h - self.size { return 0; }

        let mut max_y = 0;
        for j in self.y + self.size..self.h {
            for i in self.x..self.x + self.size {
                if arena[i as usize][j as usize] { return max_y; }
            }
            max_y = max_y + 1;
        }
        max_y
    }

    fn get_free_move_y_minus(&mut self, arena: &mut Vec<Vec<bool>>) -> i32 {
        if self.y == 0 { return 0; }

        let mut max_y = 0;
        for j in (0..self.y).rev() {
            for i in self.x..self.x + self.size {
                if arena[i as usize][j as usize] {
                    return max_y;
                }
            }
            max_y = max_y + 1;
        }
        max_y
    }

    fn meter_to_pixel(&mut self, distance: f32) -> f32 {
        distance * 1000.0
    }

    pub fn update_pos(&mut self, pitch: i32, roll: i32, accel_x: f32, accel_y: f32, arena: &mut Vec<Vec<bool>>, time_millis: u64) {
        let x = self.x;
        let y = self.y;
        let size = self.size;
        let w = self.w;
        let h = self.h;
        let mut accel_x = accel_x;
        let mut accel_y = accel_y;

        // let duration_millis = time_millis - self.prev_time_millis;
        // let duration_secs = duration_millis as f32 / 1000.0;
        // let diff_accel_x = accel_x - self.prev_accel_x).abs();
        // let diff_accel_y = (accel_y - self.prev_accel_y).abs();
        //
        // if diff_accel_x > 0.1 || diff_accel_y > 0.1 {
        //     self.prev_time_millis = time_millis;
        // }
        // s = 1/2 * a * t^2 - meter
        // let distance_x = 0.5 * accel_x * duration_secs.powf(2.0);
        // let distance_y = 0.5 * accel_y * duration_secs.powf(2.0);
        //
        // let vel_x = accel_x * duration_secs;
        // let vel_y = accel_y * duration_secs;
        //
        // println!("{} {} {}", diff_accel_x, distance_x, self.meter_to_pixel(distance_x));

        self.set_arena_status(false, arena);

        let mut move_y = (self.weight * accel_y) as i32;
        let mut move_x = (self.weight * accel_x) as i32;

        if pitch > 0 {
            if self.prev_pitch < 0 {
                // direction change - calc diff between old and new accel
                accel_y = accel_y - self.prev_accel_y;
                // new accel could be smaller then last - we might get negative move_y
                move_y = (self.weight * accel_y) as i32;
            }
            if self.y != 0 {
                self.y = (self.y - move_y.min(self.get_free_move_y_minus(arena))).min(h - size).max(0);
            }
        } else if pitch < 0 {
            if self.prev_pitch > 0 {
                accel_y = accel_y - self.prev_accel_y;
                move_y = (self.weight * accel_y) as i32;
            }
            if self.y != self.h - self.size {
                self.y = (self.y + move_y.min(self.get_free_move_y_plus(arena))).min(h - size).max(0);
            }
        }

        if roll > 0 {
            if self.prev_roll < 0 {
                accel_x = accel_x - self.prev_accel_x;
                move_x = (self.weight * accel_x) as i32;
            }
            if self.x != 0 {
                self.x = (self.x - move_x.min(self.get_free_move_x_minus(arena))).min(w - size).max(0);
            }
        } else if roll < 0 {
            if self.prev_roll > 0 {
                accel_x = accel_x - self.prev_accel_x;
                move_x = (self.weight * accel_x) as i32;
            }
            if self.x != self.w - self.size {
                self.x = (self.x + move_x.min(self.get_free_move_x_plus(arena))).min(w - size).max(0);
            }
        }

        self.prev_x = x;
        self.prev_y = y;
        self.prev_accel_x = accel_x;
        self.prev_accel_y = accel_y;
        self.prev_roll = roll;
        self.prev_pitch = pitch;

        self.set_arena_status(true, arena);
    }
}