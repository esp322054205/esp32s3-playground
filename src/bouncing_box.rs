use embedded_graphics::pixelcolor::Rgb565;

pub struct Box {
    pub size: i16,
    w: i16,
    h: i16,
    pub color: Rgb565,
    pub x_speed: i16,
    pub y_speed: i16,
    pub x: i16,
    pub y: i16,
    pub prev_x: i16,
    pub prev_y: i16,
}

impl Box {
    pub fn new(width: i16, height: i16, size: i16, color: Rgb565, x_speed: i16, y_speed: i16) -> Self {
        let mut b = Box {
            size,
            w: width,
            h: height,
            color,
            x_speed,
            y_speed,
            x: 0,
            y: 0,
            prev_x: 0,
            prev_y: 0,
        };

        b.x = b.w / 2;
        b.y = b.h / 2;
        b.prev_x = b.x;
        b.prev_y = b.y;
        b
    }


    pub fn update_pos(&mut self) {
        let x = self.x;
        let y = self.y;
        let size = self.size;
        let w = self.w;
        let h = self.h;
        let x_speed = self.x_speed.abs();
        let y_speed = self.y_speed.abs();
        self.prev_x = x;
        self.prev_y = y;

        if x + size >= w - x_speed {
            self.x_speed = -x_speed;
        } else if x <= x_speed + 1 {
            self.x_speed = x_speed;
        }

        if y + size >= h - y_speed {
            self.y_speed = -y_speed;
        } else if y <= y_speed + 1 {
            self.y_speed = y_speed;
        }

        self.x = x + self.x_speed;
        self.y = y + self.y_speed;
    }

    pub fn increase_speed(&mut self) {
        self.x_speed = if self.x_speed < 0 { self.x_speed - 1 } else { self.x_speed + 1 };
        self.y_speed = if self.y_speed < 0 { self.y_speed - 1 } else { self.y_speed + 1 };
    }

    pub fn decrease_speed(&mut self) {
        self.x_speed = if self.x_speed < 0 { (self.x_speed + 1).max(-1) } else { (self.x_speed - 1).max(1) };
        self.y_speed = if self.y_speed < 0 { (self.y_speed + 1).max(-1) } else { (self.y_speed - 1).max(1) };
    }
}