use core::fmt::Write;
use core::str::from_utf8;

use embassy_net::dns::DnsSocket;
use embassy_net::Stack;
use embassy_net::tcp::client::{TcpClient, TcpClientState};
use embedded_svc::io::ErrorKind;
use esp_println::println;
use esp_wifi::wifi::{WifiDevice, WifiStaDevice};
use heapless::String;
use log::error;
use reqwless::client::{HttpClient, TlsConfig, TlsVerify};
use reqwless::headers::ContentType;
use reqwless::request::Method::GET;
use reqwless::request::RequestBuilder;
use reqwless::response::Status;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct WeatherAll {
    pub current: WeatherCurrent,
    pub days: heapless::Vec<WeatherDay, 5>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct WeatherCurrent {
    pub temp: f32,
    pub wind_speed: f32,
    pub humidity: f32,
    pub pressure: f32,
    pub weather: heapless::Vec<WeatherDescription, 1>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct WeatherDay {
    pub temp: WeatherDayTemp,
    pub weather: heapless::Vec<WeatherDescription, 1>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct WeatherDayTemp {
    pub min: f32,
    pub max: f32,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct WeatherDescription {
    pub description: String<64>,
    pub icon: String<32>,
    pub id: u16,
}

pub struct WeatherIcons;

impl WeatherIcons {
    pub fn new() -> Self {
        WeatherIcons
    }

    pub fn get_condition_icon_file(&self, icon: String<32>, id: u16) -> String<32> {
        let mapped_id = self.map_condition_icon(icon, id);
        if mapped_id == -1 {
            return String::from("ICON_NA.PNG".parse().unwrap());
        }
        let mut name = String::new();
        write!(&mut name, "ICON_{}.PNG", mapped_id).unwrap();
        name
    }

    pub fn map_condition_icon(&self, icon: String<32>, id: u16) -> i16 {
        match (id) {
            // Thunderstorms
            202 | 232 | 211 => 4,   // thunderstorm
            212 => 3,
            221 | 231 | 201 => 38,
            230 | 200 | 210 => 37,
            300 | 301 | 302 | 310 | 311 | 312 | 313 | 314 | 321 => 9,
            500 | 501 | 520 | 521 | 531 => 11,
            502 | 503 | 504 | 522 => 12,
            511 => 10,
            600 | 620 => 14,
            601 | 621 => 16,
            602 | 622 => 41,
            611 | 612 => 18,
            615 | 616 => 5,
            741 => 20,
            711 | 762 => 22,
            701 | 721 => 21,
            731 | 751 | 761 => 19,
            771 => 23,
            781 => 0,
            800 => if icon.ends_with("n") { 31 } else { 32 },
            801 => if icon.ends_with("n") { 33 } else { 34 },
            802 => 28,
            803 | 804 => if icon.ends_with("n") { 29 } else { 30 },
            900 => 0,
            901 => 1,
            902 => 2,
            903 => 25,
            904 => 36,
            905 => 24,
            906 => 17,
            _ => -1
        }
    }
}

pub struct WeatherProvider;

const WEATHER_REQUEST_URL: &str = "http://192.168.1.15:8080/";

const LAT: f32 = 47.8095;
const LON: f32 = 13.0550;

impl WeatherProvider {
    pub fn new() -> Self {
        WeatherProvider
    }

    pub async fn fetch_weather(&self, stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>) -> Result<WeatherAll, reqwless::Error> {
        let mut url: String<1024> = String::new();
        write!(url, "{}all?lat={}&lon={}", WEATHER_REQUEST_URL, LAT, LON).unwrap();

        let mut tls_read_buffer = [0; 8192];
        let mut tls_write_buffer = [0; 8192];
        let mut rx_buffer: [u8; 8192] = [0; 8192];

        let client_state = TcpClientState::<1, 1024, 1024>::new();
        let tcp_client = TcpClient::new(&stack, &client_state);
        let dns = DnsSocket::new(&stack);

        let tls_config = TlsConfig::new(123456789u64, &mut tls_read_buffer, &mut tls_write_buffer, TlsVerify::None);
        let mut http_client = HttpClient::new_with_tls(&tcp_client, &dns, tls_config);

        let mut request = http_client.request(GET, url.as_str()).await?;
        request = request.content_type(ContentType::ApplicationJson);
        let response = request.send(&mut rx_buffer).await?;
        if response.status == Status::Ok {
            let body = response.body().read_to_end().await?;
            let body_str = from_utf8(body).unwrap();
            let json: (WeatherAll, usize) = serde_json_core::from_str(body_str).unwrap();
            return Ok(json.0)
        }
        Err(reqwless::Error::Network(ErrorKind::InvalidData))
    }
}